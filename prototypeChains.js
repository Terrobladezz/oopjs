
var gold = {
    a : 1 , 
} ; 
//simple enough
console.log(gold.a) ;
//this is a shallow copy  (like a single time copy ) 
let clone = Object.assign({},gold) ;
console.log(clone) ;
gold.a = 8 ; 
// The log returns a=1 because it is not in sync with the gold object
console.log(clone.a) ;

//fallback delegation
let blue = Object.create(gold) ; 
//empty object is logged
console.log(blue) ;
//falls back to gold because blue has no property called 'a'
console.log(blue.a) ; 
//fall back does not happen as there is a property a now in blue
blue.a=7 ;
console.log(blue.a) ; 

